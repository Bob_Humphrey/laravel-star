<x-layouts.app>
  <div class="bg-gray-400 py-8">

    <div class="pb-8">
      <div class="font-roboto_bold text-black text-3xl text-center">
        {{ $name }}
      </div>
      <div class="text-black font-nunito_light text-center">
        {{ $travelDate }}
      </div>
    </div>

    <div class="w-11/12 md:w-3/4 lg:w-2/3 xl:w-1/2 mx-auto">

      @foreach ($photos as $row)

        @php
          $size = count($row) === 1 ? 'full' : 'half';
          $colSize = count($row) === 1 ? 'col-span-4' : 'col-span-2';
          switch (count($row)) {
              case 1:
                  $size = 'full';
                  $colSize = 'col-span-12';
                  break;
          
              case 2:
                  $size = 'half';
                  $colSize = 'col-span-12 md:col-span-6';
                  break;
          
              case 3:
                  $size = 'half';
                  $colSize = 'col-span-12 md:col-span-4';
                  break;
          
              default:
                  $size = 'full';
                  $colSize = 'col-span-12';
                  break;
          }
        @endphp

        <div class="grid grid-cols-12 gap-8 pb-6">

          @foreach ($row as $photo)
            @if (!is_null($photo['file']))
              <div class="{{ $colSize }}">
                <x-img :name="$photo['file']" :size="$size" format="jpg" :alt="$name" />
                <div class="note-credit">
                  {{ is_null($photo['caption']) ? '' : $photo['caption'] }}
                </div>
              </div>
            @endif
          @endforeach

        </div>

      @endforeach

    </div>

  </div>
</x-layouts.app>
