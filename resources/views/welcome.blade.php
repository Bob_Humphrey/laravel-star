@php
use Carbon\Carbon;
$tomorrow = Carbon::tomorrow();
@endphp

<x-layouts.app>

  <div class="lg:flex w-1/2 mx-auto">
    <div class="lg:w-2/5">
      <div class="hidden lg:block mb-0 ">
        <img src="{{ asset('img/dog1.png') }}" alt="Logo" />
      </div>
    </div>
    <div class="lg:w-3/5 lg:pl-12 text-lg font-nunito_regular text-gray-700 leading-normal text-justify">

      <div class="text-3xl font-nunito_medium pr-4 mb-32">
        {{ config('app.name', 'Laravel Application') }}
      </div>

      <livewire:example>

        <div class="mt-10">
          EXAMPLE BLADE-UI-KIT COMPONENT
          COUNTDOWN UNTIL MIDNIGHT
          <x-countdown :expires="$tomorrow" />
        </div>

    </div>
  </div>

</x-layouts.app>
