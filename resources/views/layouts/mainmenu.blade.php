<a href="{{ url('/') }}" class="hover:text-blue-500 lg:px-2 py-2">
  Link
</a>
<a href="{{ url('/') }}" class="hover:text-blue-500 lg:px-2 py-2">
  Link
</a>
<a href="{{ url('/') }}" class="hover:text-blue-500 lg:px-2 py-2">
  Link
</a>
<a href="{{ url('/') }}" class="hover:text-blue-500 lg:px-2 py-2">
  Link
</a>
<a href="{{ url('/') }}" class="hover:text-blue-500 lg:px-2 py-2">
  Link
</a>

@guest
  <a href="{{ route('register') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Sign Up
  </a>
  <a href="{{ route('login') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Login
  </a>
@endguest

@auth
  <a href="{{ url('/logout') }}" class="hover:text-blue-500 lg:px-2 py-2">
    Logout
  </a>
@endauth
