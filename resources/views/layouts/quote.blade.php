<div class="flex w-full justify-center bg-gray-600 text-white py-20">
  <div class="font-inter_regular text-sm">
    Watch my back and light my way<br>
    (My traveling star, my traveling star)<br>
    Watch over all of those born St. Christopher's Day<br>
    (Old road dog, young runaway)<br>
    They hunger for home but they never stay<br>
    They wait by the door<br>
    They stand and they stare<br>
    They're already out of there<br>
    They're already out of there<br><br>
    <div class="text-black">
      "My Traveling Star"<br>
      James Taylor
    </div>
  </div>
</div>
