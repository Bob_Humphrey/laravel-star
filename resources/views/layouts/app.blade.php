<x-layouts.base>
  <div class="font-nunito_regular">
    <header class="relative">
      <x-img name="star" size="wide" format="jpg" alt="My Traveling Star" />
      <a href="/">
        <div
          class="w-full absolute inset-x-auto bottom-0 font-roboto_bold text-gray-400 hover:text-red-500 text-4xl text-center pb-4">
          My Traveling Star
        </div>
      </a>
    </header>

    <main class="w-full mx-auto">

      {{ $slot }}

    </main>

    @include('layouts.quote')
    @include('layouts.footer')
  </div>
</x-layouts.base>
