  <div class="w-full sm:w-1/2 md:w-1/3 text-lg font-nunito_regular text-center px-6 mb-10">
    <div class="bg-gray-600 hover:bg-black text-white hover:text-red-500 rounded overflow-hidden">
      <a href="{{ $page }}">
        <x-img :name="$imageName" size="full" format="jpg" alt={{ $place }} />
        <div class="pt-3 pb-5">
          <div class="font-roboto_bold">{{ $place }}</div>
          <div class="font-nunito_light text-xs">{{ $travelDate }}</div>
        </div>
      </a>
    </div>
  </div>
