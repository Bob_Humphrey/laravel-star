<div class="w-full sm:flex flex-wrap bg-gray-400 py-12 mx-auto sm:px-12">
  @foreach ($places as $place)
    <x-menu-card :place="$place" />
  @endforeach
</div>
