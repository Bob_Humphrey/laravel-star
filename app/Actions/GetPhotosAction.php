<?php

namespace App\Actions;

class GetPhotosAction
{
  public static function execute($key)
  {
    $photos = [

      // Barcelona
      'barcelona' => [
        [
          ['file' => 'barcelona_0139', 'caption' => 'La Rambla'],
          ['file' => 'barcelona_0160', 'caption' => 'Cathedral of Barcelona']
        ],
        [
          ['file' => 'barcelona_0171', 'caption' => 'Parc de la Ciutadella'],
          ['file' => 'barcelona_0177', 'caption' => 'Parc de la Ciutadella']
        ],
        [
          ['file' => 'barcelona_0180', 'caption' => null],
          ['file' => 'barcelona_0182', 'caption' => null]
        ],
        [
          ['file' => 'barcelona_0183', 'caption' => null],
          ['file' => 'barcelona_0184', 'caption' => null]
        ],
        [
          ['file' => 'barcelona_0187', 'caption' => 'Sagrada Família'],
          ['file' => 'barcelona_0202', 'caption' => 'Park Güell']
        ],
        [
          ['file' => 'barcelona_0206', 'caption' => 'Park Güell'],
          ['file' => 'barcelona_0210', 'caption' => 'Park Güell']
        ],
        [
          ['file' => 'barcelona_0212', 'caption' => 'Park Güell'],
          ['file' => 'barcelona_0225', 'caption' => 'Montjuïc']
        ],
        [
          ['file' => 'barcelona_0226', 'caption' => 'Montjuïc'],
          ['file' => 'barcelona_0231', 'caption' => 'Montjuïc']
        ],
        [
          ['file' => 'barcelona_0232', 'caption' => 'Montjuïc'],
          ['file' => 'barcelona_0238', 'caption' => 'Montjuïc']
        ],
      ],

      'barcelona_2019' => [
        [
          ['file' => 'barcelona_9017', 'caption' => null]
        ],
        [
          ['file' => 'barcelona_9012', 'caption' => null],
          ['file' => 'barcelona_9013', 'caption' => null]
        ],
        [
          ['file' => 'barcelona_9005', 'caption' => null]
        ],
        [
          ['file' => 'barcelona_9009', 'caption' => null],
          ['file' => 'barcelona_9015', 'caption' => null]
        ],
      ],

      // Brooklyn
      'brooklyn' => [
        [
          ['file' => 'brooklyn_8895', 'caption' => null]
        ],
        [
          ['file' => 'brooklyn_8897', 'caption' => null]
        ],
        [
          ['file' => 'brooklyn_8899', 'caption' => null]
        ],
        [
          ['file' => 'brooklyn_8900', 'caption' => null]
        ],
        [
          ['file' => 'brooklyn_8901', 'caption' => null]
        ],
        [
          ['file' => 'brooklyn_8910', 'caption' => null]
        ],
        [
          ['file' => 'brooklyn_8913', 'caption' => null]
        ],
        [
          ['file' => 'brooklyn_8914', 'caption' => null]
        ],
        [
          ['file' => 'brooklyn_8918', 'caption' => null]
        ],
        [
          ['file' => 'brooklyn_8920', 'caption' => null]
        ],
      ],

      // Cadiz
      'cadiz' => [
        [
          ['file' => 'cadiz_3', 'caption' => null],
          ['file' => 'cadiz_13', 'caption' => null],
        ],
        [
          ['file' => 'cadiz_5', 'caption' => null],
          ['file' => 'cadiz_8', 'caption' => null],
        ],
        [
          ['file' => 'cadiz_9', 'caption' => null],
          ['file' => 'cadiz_10', 'caption' => null],
        ],
        [
          ['file' => 'cadiz_11', 'caption' => null],
          ['file' => 'cadiz_12', 'caption' => null],
        ],
        [
          ['file' => 'cadiz_14', 'caption' => null],
          ['file' => 'cadiz_15', 'caption' => null],
        ],
        [
          ['file' => 'cadiz_7', 'caption' => null],
          ['file' => null, 'caption' => null]
        ],
      ],

      // Cannes
      'cannes' => [
        [
          ['file' => 'cannes_8', 'caption' => null],
          ['file' => 'cannes_9', 'caption' => null],
        ],
        [
          ['file' => 'cannes_2', 'caption' => null],
          ['file' => 'cannes_3', 'caption' => null],
        ],
        [
          ['file' => 'cannes_7', 'caption' => "Paul McCartney's hand prints"],
          ['file' => 'cannes_10', 'caption' => null],
        ],
      ],

      // Cinque Terre
      'cinque_terre' => [
        [
          ['file' => 'cinque_terre_9222', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9224', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9227', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9228', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9234', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9231', 'caption' => null],
          ['file' => 'cinque_terre_9236', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9244', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9252', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9254', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9261', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9242', 'caption' => null],
          ['file' => 'cinque_terre_9267', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9271', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9277', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9274', 'caption' => null],
          ['file' => 'cinque_terre_9278', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9279', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9281', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9288', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9293', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9294', 'caption' => null],
          ['file' => 'cinque_terre_9307', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9303', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9304', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9305', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9311', 'caption' => null],
        ],
        [
          ['file' => 'cinque_terre_9309', 'caption' => null],
          ['file' => 'cinque_terre_9316', 'caption' => null],
        ],
      ],

      // Cleveland
      'cleveland' => [
        [
          ['file' => 'cleveland_5126', 'caption' => 'Cleveland Art Museum'],
        ],
        [
          ['file' => 'cleveland_5069', 'caption' => 'Cleveland Art Museum'],
          ['file' => 'cleveland_5076', 'caption' => 'Cleveland Art Museum'],
        ],
        [
          ['file' => 'cleveland_5080', 'caption' => 'Cleveland Art Museum'],
          ['file' => 'cleveland_5089', 'caption' => 'Cleveland Art Museum'],
        ],
        [
          ['file' => 'cleveland_5099', 'caption' => 'Cleveland Art Museum'],
          ['file' => 'cleveland_5105', 'caption' => 'Cleveland Art Museum'],
        ],
        [
          ['file' => 'cleveland_5107', 'caption' => 'Cleveland Art Museum'],
          ['file' => 'cleveland_5123', 'caption' => 'Cleveland Art Museum'],
        ],
        [
          ['file' => 'cleveland_5128', 'caption' => 'Cleveland Art Museum'],
          ['file' => 'cleveland_5129', 'caption' => 'Cleveland Art Museum'],
        ],
        [
          ['file' => 'cleveland_5109', 'caption' => 'Cleveland Art Museum'],
        ],
        [
          ['file' => 'cleveland_5133', 'caption' => 'Cleveland Art Museum'],
          ['file' => 'cleveland_5143', 'caption' => 'Cleveland Art Museum'],
        ],
        [
          ['file' => 'cleveland_5144', 'caption' => 'Cleveland Art Museum'],
          ['file' => 'cleveland_5152', 'caption' => 'Cleveland Art Museum'],
        ],
        [
          ['file' => 'cleveland_5154', 'caption' => 'Cleveland Art Museum'],
          ['file' => 'cleveland_5155', 'caption' => 'Cleveland Art Museum'],
        ],
        [
          ['file' => 'cleveland_5158', 'caption' => 'Cleveland Art Museum'],
          ['file' => 'cleveland_5162', 'caption' => 'Cleveland Art Museum'],
        ],
        [
          ['file' => 'cleveland_5132', 'caption' => 'Cleveland Art Museum'],
        ],
      ],

      // Cornerbrook
      'cornerbrook' => [
        [
          ['file' => 'cornerbrook_1', 'caption' => null]
        ]
      ],

      // Fernandina Beach
      'fernandina_beach' => [
        [
          ['file' => 'fernandina_0367', 'caption' => null]
        ],
        [
          ['file' => 'fernandina_0361', 'caption' => null],
          ['file' => 'fernandina_0369', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0371', 'caption' => null],
          ['file' => 'fernandina_0376', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0386', 'caption' => null],
          ['file' => 'fernandina_0388', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0452', 'caption' => null]
        ],
        [
          ['file' => 'fernandina_0401', 'caption' => null],
          ['file' => 'fernandina_0408', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0354', 'caption' => null],
          ['file' => 'fernandina_0355', 'caption' => null],
          ['file' => 'fernandina_0356', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0400', 'caption' => null],
          ['file' => 'fernandina_0460', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0405', 'caption' => null]
        ],
        [
          ['file' => 'fernandina_0382', 'caption' => null],
          ['file' => 'fernandina_0384', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0411', 'caption' => null],
          ['file' => 'fernandina_0424', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0456', 'caption' => null],
          ['file' => 'fernandina_0391', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0425', 'caption' => null]
        ],
        [
          ['file' => 'fernandina_0359', 'caption' => null],
          ['file' => 'fernandina_0409', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0370', 'caption' => null],
          ['file' => 'fernandina_0393', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0461', 'caption' => null],
          ['file' => 'fernandina_0395', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0380', 'caption' => null]
        ],
        [
          ['file' => 'fernandina_0392', 'caption' => null],
          ['file' => 'fernandina_0422', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0398', 'caption' => null],
          ['file' => 'fernandina_0418', 'caption' => null],
        ],
        [
          ['file' => 'fernandina_0383', 'caption' => null],
          ['file' => 'fernandina_0438', 'caption' => null],
        ],
      ],

      // Florence
      'florence' => [
        [
          ['file' => 'florence_16', 'caption' => null]
        ],
        [
          ['file' => 'florence_10', 'caption' => null],
          ['file' => 'florence_11', 'caption' => null]
        ],
        [
          ['file' => 'florence_12', 'caption' => null]
        ],
        [
          ['file' => 'florence_14', 'caption' => null]
        ],
        [
          ['file' => 'florence_15', 'caption' => null]
        ],
        [
          ['file' => 'florence_19', 'caption' => null]
        ],
        [
          ['file' => 'florence_6', 'caption' => null],
          ['file' => 'florence_34', 'caption' => null]
        ],
        [
          ['file' => 'florence_20', 'caption' => null]
        ],
        [
          ['file' => 'florence_22', 'caption' => null]
        ],
        [
          ['file' => 'florence_23', 'caption' => null]
        ],
        [
          ['file' => 'florence_46', 'caption' => 'Sienna'],
          ['file' => 'florence_50', 'caption' => 'Sienna']
        ],
        [
          ['file' => 'florence_51', 'caption' => 'Sienna']
        ],

      ],

      // Lake Como

      'lake_como' => [
        [
          ['file' => 'lake_como_9336', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9338', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9343', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9334', 'caption' => null],
          ['file' => 'lake_como_9342', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9355', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9361', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9377', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9326', 'caption' => null],
          ['file' => 'lake_como_9347', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9383', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9386', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9390', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9389', 'caption' => null],
          ['file' => 'lake_como_9397', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9392', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9394', 'caption' => null],
        ],
        [
          ['file' => 'lake_como_9321', 'caption' => null],
        ],
      ],

      // Lisbon
      'lisbon' => [
        [
          ['file' => 'lisbon_5', 'caption' => null]
        ],
        [
          ['file' => 'lisbon_8', 'caption' => null],
          ['file' => 'lisbon_9', 'caption' => null]
        ],
        [
          ['file' => 'lisbon_3', 'caption' => null]
        ],
        [
          ['file' => 'lisbon_4', 'caption' => null]
        ],
        [
          ['file' => 'lisbon_6', 'caption' => null],
          ['file' => 'lisbon_7', 'caption' => null]
        ],
        [
          ['file' => 'lisbon_1', 'caption' => null]
        ],
        [
          ['file' => 'lisbon_2', 'caption' => null]
        ],
        [
          ['file' => 'lisbon_10', 'caption' => null],
          ['file' => 'lisbon_11', 'caption' => null]
        ],
        [
          ['file' => 'lisbon_12', 'caption' => null]
        ],
        [
          ['file' => 'lisbon_14', 'caption' => null]
        ],
      ],

      // Liverpool
      'liverpool' => [
        [
          ['file' => 'liverpool_0718', 'caption' => 'Beatles statue']
        ],
        [
          ['file' => 'liverpool_0639', 'caption' => 'Royal Albert Docks'],
          ['file' => 'liverpool_0636', 'caption' => 'Museum of Liverpool']
        ],
        [
          ['file' => 'liverpool_0643', 'caption' => 'Museum of Liverpool - John and Yoko Exhibit'],
          ['file' => 'liverpool_0651', 'caption' => 'John and Yoko Exhibit'],
          ['file' => 'liverpool_0647', 'caption' => 'John and Yoko Exhibit'],
        ],
        [
          ['file' => 'liverpool_0650', 'caption' => 'John and Yoko Exhibit'],
          ['file' => 'liverpool_0654', 'caption' => 'John and Yoko Exhibit']
        ],
        [
          ['file' => 'liverpool_0652', 'caption' => 'John and Yoko Exhibit'],
          ['file' => 'liverpool_0655', 'caption' => 'John and Yoko Exhibit'],
        ],
        [
          ['file' => 'liverpool_0648', 'caption' => 'John and Yoko Exhibit'],
          ['file' => 'liverpool_0669', 'caption' => 'Beatles Story Exhibition']
        ],
        [
          ['file' => 'liverpool_0672', 'caption' => 'Beatles Story Exhibition'],
          ['file' => 'liverpool_0686', 'caption' => 'Beatles Story Exhibition'],
        ],
        [
          ['file' => 'liverpool_0631', 'caption' => 'Royal Liver Building']
        ],
        [
          ['file' => 'liverpool_0692', 'caption' => 'Penny Lane'],
          ['file' => 'liverpool_0701', 'caption' => "John Lennon's Boyhood Home"]
        ],
        [
          ['file' => 'liverpool_0713', 'caption' => 'Matthew Street'],
          ['file' => 'liverpool_0707', 'caption' => 'Matthew Street'],
          ['file' => 'liverpool_0710', 'caption' => 'Matthew Street'],
        ],
        [
          ['file' => 'liverpool_0721', 'caption' => 'Port of Liverpool Building']
        ],
        [
          ['file' => 'liverpool_0659', 'caption' => null],
          ['file' => 'liverpool_0689', 'caption' => null]
        ],
      ],

      // Llubljana
      'llubljana' => [
        [
          ['file' => 'llubljana_13', 'caption' => null]
        ],
        [
          ['file' => 'llubljana_3', 'caption' => null],
          ['file' => 'llubljana_4', 'caption' => null]
        ],
        [
          ['file' => 'llubljana_6', 'caption' => null],
          ['file' => 'llubljana_7', 'caption' => null]
        ],
        [
          ['file' => 'llubljana_9', 'caption' => null]
        ],
        [
          ['file' => 'llubljana_5', 'caption' => null],
          ['file' => 'llubljana_8', 'caption' => null]
        ],
        [
          ['file' => 'llubljana_11', 'caption' => null],
          ['file' => 'llubljana_17', 'caption' => null]
        ],
        [
          ['file' => 'llubljana_1', 'caption' => null]
        ],
        [
          ['file' => 'llubljana_14', 'caption' => null],
          ['file' => 'llubljana_15', 'caption' => null]
        ],
        [
          ['file' => 'llubljana_20', 'caption' => null],
          ['file' => 'llubljana_21', 'caption' => null]
        ],
        [
          ['file' => 'llubljana_12', 'caption' => null]
        ],
        [
          ['file' => 'llubljana_2', 'caption' => null],
          ['file' => 'llubljana_18', 'caption' => null]
        ],
      ],

      // Memphis
      'memphis' => [
        [
          ['file' => 'memphis_1', 'caption' => 'Civil Rights Museum'],
          ['file' => 'memphis_3', 'caption' => 'Civil Rights Museum']
        ],
        [
          ['file' => 'memphis_4', 'caption' => 'Civil Rights Museum'],
          ['file' => 'memphis_5', 'caption' => 'Civil Rights Museum']
        ],
        [
          ['file' => 'memphis_6', 'caption' => 'Civil Rights Museum'],
          ['file' => 'memphis_7', 'caption' => 'Civil Rights Museum']
        ],
        [
          ['file' => 'memphis_8', 'caption' => 'Civil Rights Museum'],
          ['file' => 'memphis_9', 'caption' => 'Sun Records']
        ],
        [
          ['file' => 'memphis_10', 'caption' => 'Sun Records'],
          ['file' => 'memphis_11', 'caption' => 'Sun Records']
        ],
        [
          ['file' => 'memphis_12', 'caption' => 'Sun Records'],
          ['file' => 'memphis_13', 'caption' => 'Sun Records']
        ],
        [
          ['file' => 'memphis_14', 'caption' => 'Sun Records'],
          ['file' => 'memphis_15', 'caption' => 'Sun Records']
        ],
        [
          ['file' => 'memphis_16', 'caption' => 'Sun Records'],
          ['file' => 'memphis_23', 'caption' => 'Beale Street']
        ],
        [
          ['file' => 'memphis_17', 'caption' => 'Sun Records'],
          ['file' => 'memphis_18', 'caption' => 'Sun Records']
        ],
        [
          ['file' => 'memphis_19', 'caption' => 'Sun Records'],
          ['file' => 'memphis_20', 'caption' => 'Beale Street']
        ],
        [
          ['file' => 'memphis_21', 'caption' => 'Beale Street'],
          ['file' => 'memphis_24', 'caption' => 'Beale Street']
        ],
        [
          ['file' => 'memphis_22', 'caption' => 'Beale Street'],
          ['file' => 'memphis_25', 'caption' => 'Beale Street']
        ],
      ],

      // Pisa
      'pisa' => [
        [
          ['file' => 'pisa_9048', 'caption' => null]
        ],
        [
          ['file' => 'pisa_9049', 'caption' => null],
          ['file' => 'pisa_9050', 'caption' => null]
        ],
        [
          ['file' => 'pisa_9061', 'caption' => 'Livorno']
        ],
        [
          ['file' => 'pisa_9057', 'caption' => null],
          ['file' => 'pisa_9062', 'caption' => 'Livorno']
        ],
      ],

      // Ponta Delgada
      'ponta_delgada' => [
        [
          ['file' => 'ponta_delgada_3', 'caption' => null]
        ],
        [
          ['file' => 'ponta_delgada_1', 'caption' => null],
          ['file' => 'ponta_delgada_2', 'caption' => null]
        ],
        [
          ['file' => 'ponta_delgada_4', 'caption' => null],
          ['file' => 'ponta_delgada_5', 'caption' => null]
        ],
        [
          ['file' => 'ponta_delgada_6', 'caption' => null]
        ],
      ],

      // Rome
      'rome' => [
        [
          ['file' => 'rome_9080', 'caption' => null]
        ],
        [
          ['file' => 'rome_9065', 'caption' => null],
          ['file' => 'rome_9066', 'caption' => null]
        ],
        [
          ['file' => 'rome_9067', 'caption' => null],
          ['file' => 'rome_9069', 'caption' => null]
        ],
        [
          ['file' => 'rome_9071', 'caption' => null]
        ],
        [
          ['file' => 'rome_9075', 'caption' => null],
          ['file' => 'rome_9083', 'caption' => null]
        ],
        [
          ['file' => 'rome_9077', 'caption' => null],
          ['file' => 'rome_9084', 'caption' => null]
        ],
        [
          ['file' => 'rome_9111', 'caption' => null]
        ],
        [
          ['file' => 'rome_9091', 'caption' => null],
          ['file' => 'rome_9102', 'caption' => null]
        ],
        [
          ['file' => 'rome_9094', 'caption' => null],
          ['file' => 'rome_9105', 'caption' => null]
        ],
        [
          ['file' => 'rome_9124', 'caption' => null]
        ],
        [
          ['file' => 'rome_9095', 'caption' => null],
          ['file' => 'rome_9096', 'caption' => null]
        ],
        [
          ['file' => 'rome_9113', 'caption' => null],
          ['file' => 'rome_9106', 'caption' => null]
        ],
        [
          ['file' => 'rome_9129', 'caption' => null]
        ],
        [
          ['file' => 'rome_9119', 'caption' => null],
          ['file' => 'rome_9120', 'caption' => null]
        ],
        [
          ['file' => 'rome_9122', 'caption' => null],
          ['file' => 'rome_9127', 'caption' => null]
        ],
        [
          ['file' => 'rome_9139', 'caption' => null]
        ],
      ],

      //Salzburg
      'salzburg' => [
        [
          ['file' => 'salzburg_9675', 'caption' => null]
        ],
        [
          ['file' => 'salzburg_9684', 'caption' => null]
        ],
        [
          ['file' => 'salzburg_9674', 'caption' => null],
          ['file' => 'salzburg_9705', 'caption' => null]
        ],
        [
          ['file' => 'salzburg_9688', 'caption' => null]
        ],
        [
          ['file' => 'salzburg_9692', 'caption' => null]
        ],
        [
          ['file' => 'salzburg_9678', 'caption' => null],
          ['file' => 'salzburg_9698', 'caption' => null]
        ],
        [
          ['file' => 'salzburg_9700', 'caption' => null]
        ],
        [
          ['file' => 'salzburg_9724', 'caption' => null]
        ],
        [
          ['file' => 'salzburg_9708', 'caption' => null],
          ['file' => 'salzburg_9709', 'caption' => null]
        ],
        [
          ['file' => 'salzburg_9726', 'caption' => null]
        ],
      ],

      // Savannah
      'savannah' => [
        [
          ['file' => 'savannah_0336', 'caption' => null]
        ],
        [
          ['file' => 'savannah_0350', 'caption' => null],
          ['file' => 'savannah_0338', 'caption' => null]
        ],
        [
          ['file' => 'savannah_0326', 'caption' => null],
          ['file' => 'savannah_0345', 'caption' => null]
        ],
        [
          ['file' => 'savannah_0348', 'caption' => null]
        ],
        [
          ['file' => 'savannah_0338', 'caption' => null],
          ['file' => 'savannah_0339', 'caption' => null]
        ],
        [
          ['file' => 'savannah_0332', 'caption' => null],
          ['file' => 'savannah_0327', 'caption' => null]
        ],
        [
          ['file' => 'savannah_0349', 'caption' => null]
        ],
        [
          ['file' => 'savannah_0347', 'caption' => null],
          ['file' => 'savannah_0337', 'caption' => null]
        ],
        [
          ['file' => 'savannah_0317', 'caption' => null],
          ['file' => 'savannah_0320', 'caption' => null]
        ],
        [
          ['file' => 'savannah_0352', 'caption' => null]
        ],
        [
          ['file' => 'savannah_0340', 'caption' => null],
          ['file' => 'savannah_0344', 'caption' => null]
        ],
        [
          ['file' => 'savannah_0341', 'caption' => null],
          ['file' => 'savannah_0353', 'caption' => null]
        ],
      ],

      // Trieste
      'trieste' => [
        [
          ['file' => 'trieste_9518', 'caption' => null]
        ],
        [
          ['file' => 'trieste_9490', 'caption' => null],
          ['file' => 'trieste_9495', 'caption' => null]
        ],
        [
          ['file' => 'trieste_9497', 'caption' => null]
        ],
        [
          ['file' => 'trieste_9539', 'caption' => null],
          ['file' => 'trieste_9516', 'caption' => null]
        ],
        [
          ['file' => 'trieste_9502', 'caption' => null]
        ],
        [
          ['file' => 'trieste_9514', 'caption' => null],
          ['file' => 'trieste_9521', 'caption' => 'Muggia']
        ],
        [
          ['file' => 'trieste_9503', 'caption' => null]
        ],
        [
          ['file' => 'trieste_9535', 'caption' => null],
          ['file' => 'trieste_9529', 'caption' => 'Muggia']
        ],
        [
          ['file' => 'trieste_9525', 'caption' => 'Muggia']
        ],
        [
          ['file' => 'trieste_9527', 'caption' => 'Muggia']
        ],
        [
          ['file' => 'trieste_9563', 'caption' => 'Castello di Miramare'],
          ['file' => 'trieste_9547', 'caption' => 'Castello di Miramare']
        ],
        [
          ['file' => 'trieste_9550', 'caption' => 'Castello di Miramare'],
          ['file' => 'trieste_9558', 'caption' => 'Castello di Miramare']
        ],
        [
          ['file' => 'trieste_9538', 'caption' => null]
        ],
        [
          ['file' => 'trieste_9573', 'caption' => 'Risiera di San Sabba'],
          ['file' => 'trieste_9579', 'caption' => 'Risiera di San Sabba']
        ],
        [
          ['file' => 'trieste_9575', 'caption' => 'Risiera di San Sabba']
        ],
        [
          ['file' => 'trieste_9536', 'caption' => null],
          ['file' => 'trieste_9580', 'caption' => null]
        ],
      ],

      // Venice
      'venice' => [
        [
          ['file' => 'venice_40', 'caption' => null]
        ],
        [
          ['file' => 'venice_14', 'caption' => null],
          ['file' => 'venice_15', 'caption' => null]
        ],
        [
          ['file' => 'venice_11', 'caption' => null],
          ['file' => 'venice_17', 'caption' => null]
        ],
        [
          ['file' => 'venice_23a', 'caption' => null],
          ['file' => 'venice_33a', 'caption' => null]
        ],
        [
          ['file' => 'venice_1', 'caption' => null],
          ['file' => 'venice_2', 'caption' => null]
        ],
        [
          ['file' => 'venice_3', 'caption' => null],
          ['file' => 'venice_8', 'caption' => null]
        ],
        [
          ['file' => 'venice_20a', 'caption' => null]
        ],
        [
          ['file' => 'venice_21', 'caption' => null],
          ['file' => 'venice_22', 'caption' => null]
        ],
        [
          ['file' => 'venice_25', 'caption' => null],
          ['file' => 'venice_26', 'caption' => null]
        ],
        [
          ['file' => 'venice_24a', 'caption' => null]
        ],
        [
          ['file' => 'venice_29', 'caption' => null],
          ['file' => 'venice_31', 'caption' => null]
        ],
        [
          ['file' => 'venice_13', 'caption' => null],
          ['file' => 'venice_19', 'caption' => null]
        ],
        [
          ['file' => 'venice_18', 'caption' => null]
        ],
        [
          ['file' => 'venice_28', 'caption' => null],
          ['file' => 'venice_36', 'caption' => null]
        ],
        [
          ['file' => 'venice_38', 'caption' => null],
          ['file' => 'venice_39', 'caption' => null]
        ],
        [
          ['file' => 'venice_16a', 'caption' => null],
          ['file' => 'venice_4a', 'caption' => null]
        ],
        [
          ['file' => 'venice_30', 'caption' => null],
          ['file' => 'venice_35', 'caption' => null]
        ],
        [
          ['file' => 'venice_12', 'caption' => null],
          ['file' => 'venice_34', 'caption' => null]
        ],
        [
          ['file' => 'venice_6', 'caption' => null],
          ['file' => 'venice_7', 'caption' => null]
        ],
        [
          ['file' => 'venice_5', 'caption' => null],
          ['file' => 'venice_10', 'caption' => null]
        ],
      ],

      // Wurzburg
      'wurzburg' => [
        [
          ['file' => 'wurzburg_0121', 'caption' => 'Fortress Marienberg']
        ],
        [
          ['file' => 'wurzburg_0113', 'caption' => 'Wurzburg Residence'],
          ['file' => 'wurzburg_0135', 'caption' => 'Main River']
        ],
        [
          ['file' => 'wurzburg_0111', 'caption' => 'Peace Church'],
          ['file' => 'wurzburg_0126', 'caption' => null]
        ],
        [
          ['file' => 'wurzburg_0129', 'caption' => null]
        ],
        [
          ['file' => 'wurzburg_0118', 'caption' => 'Court Chapel in the Würzburg Residence'],
          ['file' => 'wurzburg_0146', 'caption' => 'Käppele Sanctuary']
        ],
        [
          ['file' => 'wurzburg_0143', 'caption' => 'Fortress Marienberg']
        ],
        [
          ['file' => 'wurzburg_0133', 'caption' => 'Main River'],
          ['file' => 'wurzburg_0145', 'caption' => null]
        ],
      ],

      // Zermatt
      'zermatt' => [
        [
          ['file' => 'zermatt_09a', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_02', 'caption' => null],
          ['file' => 'zermatt_03', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_30', 'caption' => null],
          ['file' => 'zermatt_31', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_06', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_04', 'caption' => null],
          ['file' => 'zermatt_05', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_10', 'caption' => null],
          ['file' => 'zermatt_13', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_14', 'caption' => null],
          ['file' => 'zermatt_15', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_29', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_32', 'caption' => null],
          ['file' => 'zermatt_33', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_12', 'caption' => null],
          ['file' => 'zermatt_35', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_19', 'caption' => null],
          ['file' => 'zermatt_28', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_36', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_11', 'caption' => null],
          ['file' => 'zermatt_26', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_20', 'caption' => null],
          ['file' => 'zermatt_21', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_17', 'caption' => null],
          ['file' => 'zermatt_18', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_37', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_22', 'caption' => null],
          ['file' => 'zermatt_23', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_24', 'caption' => null],
          ['file' => 'zermatt_25', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_16', 'caption' => null],
          ['file' => 'zermatt_34', 'caption' => null]
        ],
        [
          ['file' => 'zermatt_01', 'caption' => null],
          ['file' => 'zermatt_08', 'caption' => null]
        ],
      ],

    ];

    return $photos[$key];
  }
}
