<?php

namespace App\Actions;

class GetPlacesAction
{
  public static function execute()
  {
    return array_reverse([
      [
        'key' => 'zermatt',
        'place' => null,
        'travelDate' => 'August 1992',
        'imageName' => 'zermatt_0'
      ],
      [
        'key' => 'savannah',
        'place' => null,
        'travelDate' => 'March 28, 2003',
        'imageName' => 'savannah_0'
      ],
      [
        'key' => 'fernandina_beach',
        'place' => 'Fernandina Beach',
        'travelDate' => 'March 2003',
        'imageName' => 'fernandina_0'
      ],
      [
        'key' => 'cleveland',
        'place' => null,
        'travelDate' => 'December 3, 2015',
        'imageName' => 'cleveland_0'
      ],
      [
        'key' => 'barcelona',
        'place' => null,
        'travelDate' => 'April 17-20, 2018',
        'imageName' => 'barcelona_0'
      ],
      [
        'key' => 'memphis',
        'place' => null,
        'travelDate' => 'November 2018',
        'imageName' => 'memphis_0'
      ],
      [
        'key' => 'brooklyn',
        'place' => null,
        'travelDate' => 'April 27, 2019',
        'imageName' => 'brooklyn_8913'
      ],
      [
        'key' => 'ponta_delgada',
        'place' => 'Ponta Delgada',
        'travelDate' => 'May 4, 2019',
        'imageName' => 'ponta_delgada_3'
      ],
      [
        'key' => 'lisbon',
        'place' => null,
        'travelDate' => 'May 6, 2019',
        'imageName' => 'lisbon_5'
      ],
      [
        'key' => 'cadiz',
        'place' => null,
        'travelDate' => 'May 7, 2019',
        'imageName' => 'cadiz_13'
      ],
      [
        'key' => 'barcelona_2019',
        'place' => 'Barcelona',
        'travelDate' => 'May 9, 2019',
        'imageName' => 'barcelona_9017'
      ],
      [
        'key' => 'cannes',
        'place' => null,
        'travelDate' => 'May 10, 2019',
        'imageName' => 'cannes_8'
      ],
      [
        'key' => 'pisa',
        'place' => null,
        'travelDate' => 'May 11, 2019',
        'imageName' => 'pisa_9048'
      ],
      [
        'key' => 'rome',
        'place' => null,
        'travelDate' => 'May 12-16, 2019',
        'imageName' => 'rome_9080'
      ],
      [
        'key' => 'florence',
        'place' => null,
        'travelDate' => 'May 16-20, 2019',
        'imageName' => 'florence_16'
      ],
      [
        'key' => 'cinque_terre',
        'place' => 'Cinque Terre',
        'travelDate' => 'May 21-23, 2019',
        'imageName' => 'cinque_terre_9303'
      ],
      [
        'key' => 'lake_como',
        'place' => 'Lake Como',
        'travelDate' => 'May 24-26, 2019',
        'imageName' => 'lake_como_9355'
      ],
      [
        'key' => 'venice',
        'place' => null,
        'travelDate' => 'May 27-29, 2019',
        'imageName' => 'venice_40'
      ],
      [
        'key' => 'trieste',
        'place' => null,
        'travelDate' => 'May 30-June 3, 2019',
        'imageName' => 'trieste_9518'
      ],
      [
        'key' => 'llubljana',
        'place' => null,
        'travelDate' => 'June 4-6, 2019',
        'imageName' => 'llubljana_13'
      ],
      [
        'key' => 'salzburg',
        'place' => null,
        'travelDate' => 'June 7-10, 2019',
        'imageName' => 'salzburg_9726'
      ],
      [
        'key' => 'wurzburg',
        'place' => null,
        'travelDate' => 'June 22-24, 2019',
        'imageName' => 'wurzburg_0121'
      ],
      [
        'key' => 'liverpool',
        'place' => null,
        'travelDate' => 'July 16, 2019',
        'imageName' => 'liverpool_0718'
      ],
      [
        'key' => 'cornerbrook',
        'place' => 'Cornerbrook, Newfoundland',
        'travelDate' => 'July 24, 2019',
        'imageName' => 'cornerbrook_1'
      ],
    ]);
  }
}
