<?php

namespace App\Http\Controllers;

use Illuminate\Support\Str;
use App\Actions\GetPhotosAction;
use App\Actions\GetPlacesAction;

class PlaceController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function show($place)
  {
    $path  =  request()->path();
    $places = GetPlacesAction::execute();
    $collection = collect($places);
    $place = $collection->firstWhere('key', $path);
    $name = is_null($place['place']) ? Str::of($place['key'])->title() : $place['place'];
    $travelDate = $place['travelDate'];
    $photos = GetPhotosAction::execute($path);
    return view('place', [
      'name' => $name,
      'travelDate' => $travelDate,
      'photos' => $photos
    ]);
  }
}
