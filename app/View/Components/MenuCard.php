<?php

namespace App\View\Components;

use Illuminate\View\Component;
use Illuminate\Support\Str;

class MenuCard extends Component
{
  public $imageName;
  public $page;
  public $format;
  public $src;
  public $alt;
  public $place;
  public $travelDate;
  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($place)
  {
    $this->imageName = $place['imageName'];
    $this->format = 'jpg';
    $this->src = '/img/' . $place['imageName'] . '.jpg';
    $placeName = is_null($place['place']) ? Str::of($place['key'])->title() : $place['place'];
    $this->place = $placeName;
    $this->alt = $placeName;
    $page = $place['key'];
    $this->page = '/' . $page;
    $this->travelDate = $place['travelDate'];
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\Contracts\View\View|string
   */
  public function render()
  {
    return view('components.menu-card');
  }
}
