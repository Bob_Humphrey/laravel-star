<?php

namespace App\View\Components;

use Illuminate\View\Component;

class Img extends Component
{
  public $name;
  public $format;
  public $src;
  public $alt;

  /**
   * Create a new component instance.
   *
   * @return void
   */
  public function __construct($name, $size, $format, $alt)
  {
    $this->name = $name;
    $this->format = $format;
    $this->src = '/img/originals/' . $size . '/' . $name . '.' . $format;
    $this->alt = $alt;
  }

  /**
   * Get the view / contents that represent the component.
   *
   * @return \Illuminate\Contracts\View\View|string
   */
  public function render()
  {
    return view('components.img');
  }
}
